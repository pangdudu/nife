class CreateTimeIntervals < ActiveRecord::Migration
  def self.up
    create_table :time_intervals do |t|
      t.datetime :start, :default => Time.now
      t.datetime :stop, :default => nil
      t.integer :task_id

      t.timestamps
    end
  end

  def self.down
    drop_table :time_intervals
  end
end
