# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_nife_session',
  :secret      => 'f5b532cdea51fe511acfade5b86052201a671dfbc692dbfc7493c0f1fc906dc6c3557b9589ac27233529fc809f7e5b37acf06f63d3764b3a17fd6c6d00fc7535'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
