class Task < ActiveRecord::Base
  has_many :time_intervals, :dependent => :destroy
  
  # virtual attributes
  
  def start
    self.stop if self.started?
    self.time_intervals.create(:start => Time.now)
  end
  
  def pause
    unless self.paused?
      self.stop
      self.time_intervals.create(:start => nil, :stop => nil)
    end 
  end
  
  def stop
    self.time_intervals.all(:conditions => { :stop => nil }).each do |ti|
      ti.update_attribute(:start, Time.now) if ti.start.nil?
      ti.update_attribute(:stop, Time.now)
    end
  end
  
  def started?
    return self.time_intervals.exists?(:stop => nil)
  end
  
  def paused?
    return self.time_intervals.exists?(:start => nil, :stop => nil)
  end
  
  def stopped?
    return (self.time_intervals.empty? || !self.started?)
  end
  
end
