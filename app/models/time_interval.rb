class TimeInterval < ActiveRecord::Base
  belongs_to  :task
   
  # virtual attributes
  def duration
    delta = 0
    delta = self.stop - self.start unless self.stop.nil? || self.start.nil?
    return delta
  end
   
end
